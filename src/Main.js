
import React, { useState, useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import PostsList from './posts/PostsList';
import LikesCounter from './LikesCounter';
import Content from './layout/Content';
import Sider from './layout/Sider';
import UserAvatar from './user/UserAvatar';
import { setUser } from './redux/actions/user.actions';
import { getUser } from './redux/effects/user.effects';
import UserForm from './user/UserForm';
import { Layout, Menu } from 'antd';

const postsData = [
    {
      id: 1,
      title: 'My first post',
      text: 'New Post in the blog',
      liked: false,
    },
    {
      id: 2,
      title: 'The second post',
      text: 'Awesome text!',
      liked: false,
    },
    {
      id: 3,
      title: 'Another post',
      text: 'Hello World',
      liked: false,
    }
  ];

const Main = ({ userData, getUser, isLoading }) => {
    
    console.log(userData)
    const [posts, setPosts] = useState(postsData);
    const [likedCount, setLikedCount] = useState(0);
  
    useEffect(() => {
      setLikedCount(posts.filter(p => p.liked).length);
    }, [posts]);
  
    const onLikedClicked = (postId) => {
      const newPosts = posts.map(post => {
        if (postId === post.id) {
          post.liked = !post.liked;
        }
        return post;
      });
      setPosts(newPosts);
    }
  
    const onNameChangeClick = useCallback(() => {
      
    });

    return (
       <Layout>
          <Layout.Header>
          </Layout.Header>
          <Layout>
            <Layout.Sider>
              <Menu>
                <Menu.Item>
                  Test Link
                </Menu.Item>
              </Menu>
            </Layout.Sider>
            <Layout>
              <Layout.Content>
                <UserForm />
              </Layout.Content>
            </Layout>
          </Layout>
       </Layout>
    );
}

const mapStateToProps = state => ({
    userData: state.user.userData,
    isLoading: state.user.loading
});

export default connect(mapStateToProps, { setUser, getUser })(Main);